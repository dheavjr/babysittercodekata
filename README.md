# Darin's Babysitter Calculator Code Kata
## How to build and run tests
***

After checking out the project from the Bitbucket repo (https://bitbucket.org/dheavjr/babysittercodekata), please run the following command to execute the tests
    
    mvn test

Darin Heavilin - darinheavilinjr@gmail.com