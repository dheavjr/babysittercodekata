package babysitter;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static junit.framework.TestCase.assertEquals;

public class BabySitterCalculatorTest {

    private BabySitterCalculator babySitterCalculator;

    private final SimpleDateFormat format = new SimpleDateFormat("hhmm");

    @Before
    public void setup() {
        babySitterCalculator = new BabySitterCalculator();
    }

    @Test
    public void itReturns12ForOneHour() throws ParseException {
        DateTime startTime = new DateTime(format.parse("1700"));
        DateTime endTime = new DateTime(format.parse("1800"));
        int actual = babySitterCalculator.calculatePay(startTime, null, endTime);
        assertEquals(12, actual);
    }

    @Test
    public void itReturns24ForTwoHours() throws ParseException {
        DateTime startTime = new DateTime(format.parse("1700"));
        DateTime endTime = new DateTime(format.parse("1900"));
        int actual = babySitterCalculator.calculatePay(startTime, null, endTime);
        assertEquals(24, actual);
    }

    @Test
    public void itReturns20ForOneHourBeforeBedtimeAndOneHourAfterBedtime() throws ParseException {
        DateTime startTime = new DateTime(format.parse("1700"));
        DateTime bedTime = new DateTime(format.parse("1800"));
        DateTime endTime = new DateTime(format.parse("1900"));
        int actual = babySitterCalculator.calculatePay(startTime, bedTime, endTime);
        assertEquals(20, actual);
    }

    @Test
    public void itReturns36ForThreeHoursAndFiftyNineMinutes() throws ParseException {
        DateTime startTime = new DateTime(format.parse("1700"));
        DateTime endTime = new DateTime(format.parse("2059"));
        int actual = babySitterCalculator.calculatePay(startTime, null, endTime);
        assertEquals(36, actual);
    }

    @Test
    public void itReturns28For11pmTo1am() throws ParseException {
        DateTime startTime = new DateTime(format.parse("2300"));
        DateTime endTime = new DateTime(format.parse("0100")).plusDays(1);
        int actual = babySitterCalculator.calculatePay(startTime, null, endTime);
        assertEquals(28, actual);
    }

    @Test
    public void itReturns52For10pmTo2amWithBedTimeAt11pm() throws ParseException {
        DateTime startTime = new DateTime(format.parse("2200"));
        DateTime bedTime = new DateTime(format.parse("2300"));
        DateTime endTime = new DateTime(format.parse("0200")).plusDays(1);
        int actual = babySitterCalculator.calculatePay(startTime, bedTime, endTime);
        assertEquals(52, actual);
    }

    @Test
    public void itReturns28For11pmTo101am() throws ParseException {
        DateTime startTime = new DateTime(format.parse("2300"));
        DateTime endTime = new DateTime(format.parse("0101")).plusDays(1);
        int actual = babySitterCalculator.calculatePay(startTime, null, endTime);
        assertEquals(28, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void itThrowsAnIllegalArgumentExceptionWhenStartDateIsBefore5pm() throws ParseException {
        DateTime startTime = new DateTime(format.parse("1659"));
        DateTime endTime = new DateTime(format.parse("1800"));
        babySitterCalculator.calculatePay(startTime, null, endTime);
    }

    @Test(expected = IllegalArgumentException.class)
    public void itThrowsAnIllegalArgumentExceptionWhenEndDateIsAfter4am() throws ParseException {
        DateTime startTime = new DateTime(format.parse("1800"));
        DateTime endTime = new DateTime(format.parse("0401")).plusDays(1);
        babySitterCalculator.calculatePay(startTime, null, endTime);
    }

    @Test(expected = IllegalArgumentException.class)
    public void itThrowsAnIllegalArgumentExceptionWhenStartTimeIsNull() throws ParseException {
        babySitterCalculator.calculatePay(null, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void itThrowsAnIllegalArgumentExceptionWhenEndTimeIsNull() throws ParseException {
        DateTime startTime = new DateTime(format.parse("1800"));
        babySitterCalculator.calculatePay(startTime, null, null);
    }
}
