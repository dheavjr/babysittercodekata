package babysitter;

import org.joda.time.DateTime;
import org.joda.time.Hours;

import java.text.ParseException;
import java.text.SimpleDateFormat;

class BabySitterCalculator {

    private static final String HHMM = "hhmm";
    private static final String FOUR_AM = "0400";
    private static final String FIVE_PM = "1700";
    private static final String START_TIME_BEFORE_5_PM_ERROR_MESSAGE = "Start time cannot be before 5PM";
    private static final String END_TIME_AFTER_4_AM_ERROR_MESSAGE = "End time cannot be after 4AM";
    private static final int BED_TIME_RATE = 8;
    private static final int NORMAL_RATE = 12;
    private static final int AFTER_MIDNIGHT_RATE = 16;
    private static final String START_TIME_REQUIRED_ERROR_MESSAGE = "A start time must be provided";
    private static final String END_TIME_REQUIRED_ERROR_MESSAGE = "An end time mut be provided";
    private static final SimpleDateFormat format = new SimpleDateFormat(HHMM);

    int calculatePay(final DateTime startTime, final DateTime bedTime, final DateTime endTime) throws ParseException {
        validateInput(startTime, endTime);

        DateTime midnight = new DateTime(startTime).plusDays(1).withTimeAtStartOfDay();
        if (bedTime != null) {
            return calculatePayWithBedTime(startTime, bedTime, endTime, midnight);
        }
        if (endTime.isAfter(midnight)) {
            return calculatePayWithRate(startTime, midnight, NORMAL_RATE) + calculatePayWithRate(midnight, endTime, AFTER_MIDNIGHT_RATE);
        }
        return calculatePayWithRate(startTime, endTime, NORMAL_RATE);
    }

    private void validateInput(final DateTime startTime, final DateTime endTime) throws ParseException {
        checkRequiredField(startTime, START_TIME_REQUIRED_ERROR_MESSAGE);
        checkRequiredField(endTime, END_TIME_REQUIRED_ERROR_MESSAGE);
        validateStartTime(startTime);
        validateEndTime(endTime);
    }

    private void validateEndTime(DateTime endTime) throws ParseException {
        DateTime fourAm = new DateTime(format.parse(FOUR_AM)).plusDays(1);
        if (endTime.isAfter(fourAm)) {
            throw new IllegalArgumentException(END_TIME_AFTER_4_AM_ERROR_MESSAGE);
        }
    }

    private void validateStartTime(DateTime startTime) throws ParseException {
        DateTime fivePm = new DateTime(format.parse(FIVE_PM));
        if (startTime.isBefore(fivePm)) {
            throw new IllegalArgumentException(START_TIME_BEFORE_5_PM_ERROR_MESSAGE);
        }
    }

    private void checkRequiredField(final Object o, final String errorMessage) {
        if (o == null) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    private int calculatePayWithBedTime(final DateTime startTime, final DateTime bedTime, final DateTime endTime, final DateTime midnight) {
        if (endTime.isAfter(midnight)) {
            return calculatePayWithRate(startTime, bedTime, NORMAL_RATE)
                    + calculatePayWithRate(bedTime, midnight, BED_TIME_RATE)
                    + calculatePayWithRate(midnight, endTime, AFTER_MIDNIGHT_RATE);
        }
        return calculatePayWithRate(startTime, bedTime, NORMAL_RATE) + calculatePayWithRate(bedTime, endTime, BED_TIME_RATE);
    }

    private int calculatePayWithRate(final DateTime startTime, final DateTime endTime, final int rate) {
        return Hours.hoursBetween(startTime, endTime).getHours() * rate;
    }
}
